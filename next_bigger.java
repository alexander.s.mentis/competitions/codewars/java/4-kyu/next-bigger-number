import java.util.Arrays;

public class Kata
{
    private static int indexOfLarger(char[] digits, int startAt) {
      char toBeat = digits[startAt];
      char smallest = digits[startAt+1];
      int index = -1;
      for (int i = startAt + 1; i < digits.length; i++) {
        if (digits[i] > toBeat && digits[i] <= smallest) {
          smallest = digits[i];
          index = i;
        }
      }
      return index;
    }
  
    private static void swap(char[] digits, int a, int b) {
      char temp = digits[a];
      digits[a] = digits[b];
      digits[b] = temp;
    }
  
    private static void buildAnswer(char[] digits, int place, int toSwap) {
      swap(digits, place, toSwap);
      Arrays.sort(digits, place+1, digits.length);
    }
  
    public static long nextBiggerNumber(long n)
    {
      char[] digits = (Long.toString(n)).toCharArray();
      
      for (int place = digits.length - 2; place >= 0; place--) {
        int toSwap = indexOfLarger(digits, place);
        if (toSwap != -1) {
          buildAnswer(digits, place, toSwap);
          return Long.parseLong(new String(digits));
        }
      }
      
      return -1; // not possible
    }
}